$(document).ready(function() {

 $('#eventtimestart').timepicker({'scrollDefault': '12:30:00' }); 
 $('#eventtimeend').timepicker({'scrollDefault': '12:30:00' }); 
 $('#pickuptimestart').timepicker({'scrollDefault': '12:30:00' });  
 $('#pickuptimeend').timepicker({'scrollDefault': '12:30:00' });  
 $('input.datepicker').Zebra_DatePicker().removeAttr("readonly");
 $('#event_form').validate();
 $('#samebilling_values').change(function() {
 		if($(this).is(':checked')){ 
	 		$('#bfname').val($('#yfname').val());
	    $('#blname').val($('#ylname').val());
	    $('#billingphone').val($('#yphone').val());
	    $('#billingcell').val($('#ycell').val());
	    $('#bemail').val($('#email').val());
	    $('#billingstreet').val($('#eventstreet').val());
	    $('#billingcity').val($('#eventcity').val());
	    $('#billingstate').val($('#eventstate').val());
	    $('#billingzip').val($('#eventzip').val());
 		}
 });
}); //End Document Ready 

